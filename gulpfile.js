var elixir = require('laravel-elixir');
var gulp = require('gulp');
var gettext = require('gulp-angular-gettext');
var concat = require('gulp-concat');

var styl = require('laravel-elixir-stylus');
var Task = elixir.Task;
var shell = require('gulp-shell');
var browsersync = require('browser-sync');
var annotate = require('laravel-elixir-ng-annotate');
var ngTemplateCache = require('laravel-elixir-ngtemplatecache');

elixir.extend('htmlWatcher', function () {
    new Task('htmlWatcher', function () {
        browsersync.reload();
        return shell('HTML Reloading');
    }).watch('public/templates/**/*.html');

});

elixir.extend('vendorScripts', function () {
    new Task('vendorScripts', function () {
        return gulp.src([
            'resources/assets/vendor/angular/angular.min.js',
            'resources/assets/vendor/angular-ui-router/release/angular-ui-router.min.js',
            'resources/assets/vendor/angular-animate/angular-animate.min.js',
            'resources/assets/vendor/angular-aria/angular-aria.min.js',
            'resources/assets/vendor/angular-material/angular-material.min.js',
            'resources/assets/vendor/angular-messages/angular-messages.min.js',
            'resources/assets/vendor/angular-gettext/dist/angular-gettext.min.js',
            'resources/assets/vendor/angular-resource/angular-resource.min.js',
            'resources/assets/vendor/angular-sanitize/angular-sanitize.min.js',
            'resources/assets/vendor/ngMeta/dist/ngMeta.min.js',
            'resources/assets/vendor/exif-js/exif.js',
            'resources/assets/vendor/lodash/dist/lodash.min.js',
            'resources/assets/vendor/fastclick/lib/fastclick.js',
            'resources/assets/vendor/moment/min/moment.min.js',
            'resources/assets/vendor/Swiper/dist/js/swiper.min.js',
           // 'resources/assets/vendor/ngmap/build/scripts/ng-map.min.js'
             'resources/assets/vendor/angular-ymaps/angular-ymaps.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('public/js/client'));
    });

});

elixir(function (mix) {

    var vendorAssetsPath = './resources/assets/vendor/';
    var jsAssetsPath = './resources/assets/js/';

    /* ============================================================================================
     ADMIN AREA
     =============================================================================================== */
    var adminCssBuildPath = elixir.config.get('public.css.outputFolder') + '/admin/';
    var adminJsBuildPath = elixir.config.get('public.js.outputFolder') + '/admin/';

    var clientCssBuildPath = elixir.config.get('public.css.outputFolder') + '/client/';
    var clientJsBuildPath = elixir.config.get('public.js.outputFolder') + '/client/';

    /* ************************************** VENDOR CSS ****************************************** */
    // compile admin user vendor.css
    mix.styles([
        'bootstrap/dist/css/bootstrap.min.css',
        'metisMenu/dist/metisMenu.min.css',
        'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        'font-awesome/css/font-awesome.min.css',
        'startbootstrap-sb-admin-2/dist/css/sb-admin-2.css',
        'jquery-ui/themes/base/jquery-ui.min.css'
    ], adminCssBuildPath + 'vendor.user.css', vendorAssetsPath);

    mix.scripts([
        'jquery/dist/jquery.min.js',
        'bootstrap/dist/js/bootstrap.min.js',
        'metisMenu/dist/metisMenu.min.js',
        'datatables/media/js/jquery.dataTables.min.js',
        'datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
        'startbootstrap-sb-admin-2/dist/js/sb-admin-2.js',
        'jquery-ui/jquery-ui.min.js',
        'jquery.cookie/jquery.cookie.js'
    ], adminJsBuildPath + 'vendor.user.js', vendorAssetsPath);

    mix.ngTemplateCache('/**/*.*', clientJsBuildPath, 'resources/assets/templates', {
        templateCache: {
            standalone: true
        },
        htmlmin: {
            collapseWhitespace: true,
            removeComments: true
        }
    });

    mix.vendorScripts();

    mix.annotate([
        jsAssetsPath + 'client/common/app.common.js',
        jsAssetsPath + 'client/authorization/app.authorization.js',
        jsAssetsPath + 'client/**/*.js'
    ], clientJsBuildPath);


    //user scripts
    mix.scripts([
        'translations.js',
        'templates.js',
        'annotated.js'
    ], clientJsBuildPath + 'user.js', clientJsBuildPath);

    //vendor styles
    mix.styles([
        'angular-material/angular-material.css',
        'Swiper/dist/css/swiper.min.css'
    ], clientCssBuildPath + 'vendor.css', vendorAssetsPath);

    // compile client stylus files
    mix.stylus('base.styl', clientCssBuildPath + 'user.css');


    mix.version([
        clientCssBuildPath + 'vendor.css',
        clientCssBuildPath + 'user.css',
        clientJsBuildPath + 'vendor.js',
        clientJsBuildPath + 'user.js'
    ]);

    mix.copy([
        vendorAssetsPath + 'font-awesome/fonts',
        vendorAssetsPath + 'bootstrap/dist/fonts'
    ], elixir.config.get('public.versioning.buildFolder') + '/css/fonts');

    gulp.task('pot', function () {
        gulp.src(['resources/assets/templates/**/*.html', 'resources/assets/js/client/**/*.js']) //
        .pipe(gettext.extract('template.pot'))
        .pipe(gulp.dest('resources/assets/po/'));
    });

    gulp.task('translations', function () {
        gulp.src('resources/assets/po/*.po')
        .pipe(gettext.compile())
        .pipe(concat('translations.js'))
        .pipe(gulp.dest('public/js/client'));
    });

    //mix.htmlWatcher();

    mix.browserSync({
            files: ['public/templates/**/*.html', 'public/build/css/**/*.css', 'public/build/js/**/*.js'],
            proxy: 'dvm.local',
            port: 8080
        }
    );

});
