(function () {
    'use strict';

    angular
        .module('dvm.main')
        .controller('MainPageController', MainPageController);

    function MainPageController($rootScope) {

        var vm = this;
        
        $rootScope.mainHeader = 'Main page header';

        vm.banners = [
            {
                'banner_id': 5,
                'button_text': "",
                'button_type': "none",
                'description': null,
                'image_lg': "/img/banner.jpg",
                'image_md': "/img/banner.jpg",
                'image_sm': "/img/banner.jpg",
                'image_xl': "/img/banner.jpg",
                'image_xs': "/img/banner.jpg",
                'target_blank': false,
                'title': "Танцы для детей от 4 лет",
                'url': ""
            },
            {
                'banner_id': 6,
                'button_text': "",
                'button_type': "none",
                'description': null,
                'image_lg': "/img/banner.jpg",
                'image_md': "/img/banner.jpg",
                'image_sm': "/img/banner.jpg",
                'image_xl': "/img/banner.jpg",
                'image_xs': "/img/banner.jpg",
                'target_blank': false,
                'title': "",
                'url': ""
            }
        ];

        vm.shortDescription = 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?';
        vm.photos = {
            left: '/img/1.jpg',
            right: '/img/2.jpg'
        };
        vm.mainPhoto = '/img/4.jpg';

        vm.directions = [
            {
                'image': '/img/dance-297494_960_720.png',
                'title': 'Акробатика'
            },
            {
                'image': '/img/tanzpaar_standard.png',
                'title': 'Бальные танцы'
            },
            {
                'image': '/img/dance-297494_960_720.png',
                'title': 'Современные'
            },
            {
                'image': '/img/tanzpaar_standard.png',
                'title': 'Стретчинг'
            },
            {
                'image': '/img/dance-297494_960_720.png',
                'title': 'Дети 14+'
            },
            {
                'image': '/img/tanzpaar_standard.png',
                'title': 'Свадебный танец'
            },
            {
                'image': '/img/tanzpaar_standard.png',
                'title': 'Стриппластика'
            },
            {
                'image': '/img/tanzpaar_standard.png',
                'title': 'Соло латина'
            }
        ];

    }

}());
