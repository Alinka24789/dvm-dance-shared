(function () {
    'use strict';

    angular
    .module('dvm.main')
    .directive('directionsDance', [function() {
        return {
            restrict: 'E',
            replace: false,
            scope: {
                directions: '='
            },
            templateUrl: 'partials/main/directions.dance.html',
            controller: function () {
                var vm = this;
            },
            controllerAs: 'direction',
            bindToController: true
        };
    }]);
}());
