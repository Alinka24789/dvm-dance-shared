(function () {
    'use strict';

    angular
        .module('dvm.main')
        .directive('mainSlider', mainSlider);

    function mainSlider(stylesheets) {
        return {
            restrict: 'E',
            replace: false,
            scope: {
                banners: '='
            },
            templateUrl: 'partials/main/slider.html',
            link: function (scope, elm) {
                var defaultParams = {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    spaceBetween: 30,
                    effect: 'fade'
                };
                var $swiperWrapper = document.querySelector('.swiper-container');
                var imagesSizeMap = {
                    "image_xs": "320px",
                    "image_sm": "768px",
                    "image_md": "1280px",
                    "image_lg": "1440px",
                    "image_xl": "1680px",
                };

                var swiperParams = _.extend({}, defaultParams, scope.params);

                //TMP FOR STATIC SLIDES
                /*setTimeout(function(){
                    var swiper = new Swiper('.swiper-container', swiperParams);
                }, 400);*/

                scope.$on('last-repeat', function () {
                    initSwiper();
                    $swiperWrapper.classList.add('ready');
                });

                function initSwiper() {
                    var sheet = stylesheets.createSheet();
                    setSliderImageStyles(sheet, scope.slider.banners, imagesSizeMap);
                    var swiper = new Swiper('.swiper-container', swiperParams);
                }

                /**
                 * Set background images styles with media queries for each slider
                 */
                function setSliderImageStyles(sheet, banners, imagesSizeMap) {
                    _.each(banners, function (banner, idx) {
                        var sliderClass = '.slide--' + idx;
                        _.each(_.keys(imagesSizeMap), function (key) {
                            var style = '@media only screen and (max-width : ' + imagesSizeMap[key] + ') { ' +
                                sliderClass + '{ background-image: url(' + banner[key] + '); } }';
                            sheet.insertRule(style, sheet.cssRules.length);
                        });
                        sheet.insertRule('@media only screen and (min-width : 1680px) { ' +
                            sliderClass + '{ background-image: url(' + banner.image_xl + '); } }', sheet.cssRules.length)
                    })
                }
            },
            controller: sliderController,
            controllerAs: 'slider',
            bindToController: true
        }
    }

    function sliderController($mdDialog, $location) {
        var vm = this;
        vm.handleButtonClick = handleButtonClick;
        vm.showVideoModal = showVideoModal;

        function handleButtonClick(banner){
            if (banner.button_type == 'video') {
                showVideoModal(banner.url);
            } else if (banner.button_type == 'link') {
                if (banner.target_blank) {
                    window.open(banner.url, '_blank');
                } else {
                    $location.path(banner.url);
                }
            }
        }

        function showVideoModal(videoUrl) {
            $mdDialog.show({
                controllerAs: 'vm',
                parent: angular.element(document.body),
                controller: function ($mdDialog, $sce, url) {
                    var vm = this;
                    var videoId;
                    if (url !== null) {
                        if (~url.indexOf('youtube') || ~url.indexOf('youtu.be')) {
                            videoId = url.match(/.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/);
                            url = 'https://www.youtube.com/embed/' + videoId[1];
                        } else if (~url.indexOf('vimeo')) {
                            videoId = url.match(/.*(?:staffpicks\/|video\/|vimeo.com\/)(\d*).*/);
                            url = 'https://player.vimeo.com/video/' + videoId[1];
                        } else {
                            that.url = '';
                            return;
                        }

                    }
                    vm.url = $sce.trustAsResourceUrl(url + '?rel=0');
                    vm.hide = hide;
                    function hide() {
                        $mdDialog.hide();
                    }
                },
                templateUrl: 'partials/modals/video.modal.html',
                clickOutsideToClose: true,
                locals: {
                    url: videoUrl
                }
            })
        }
    }

}());


