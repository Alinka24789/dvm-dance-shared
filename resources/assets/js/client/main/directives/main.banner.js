(function () {
    'use strict';

    angular
    .module('dvm.main')
    .directive('mainBanner', ['$window', function($window) {
        return {
            link: function (scope, elm) {

                var w = angular.element($window);
                var onResizeGrid = _.throttle(onResize, 100);

                init();

                function init() {
                    initBanners();
                    angular.element($window).on('resize', onResizeGrid);

                }

                function onResize() {
                    initBanners();
                }

                function initBanners() {
                    elm.css({ 'height': w[0].innerHeight + 'px'});
                    var img = elm[0].querySelector('.main-cover img');
                    var imgHeight = Math.floor(w[0].innerWidth / img.width * img.height);
                    var separatorHeight = (w[0].innerHeight - imgHeight) / 2;
                    var separator1 = angular.element(elm[0].querySelector('.cover-separator-1'));
                    var separator2 = angular.element(elm[0].querySelector('.cover-separator-2'));
                    separator1.css({ 'height': separatorHeight + 'px'});
                    separator2.css({ 'height': separatorHeight + 10 + 'px'});
                }
            }
        }
    }]);

}());
