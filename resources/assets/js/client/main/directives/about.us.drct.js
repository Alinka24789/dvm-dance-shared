(function () {
    'use strict';

    angular
    .module('dvm.main')
    .directive('aboutUs', [function() {
        return {
            restrict: 'E',
            replace: false,
            scope: {
                description: '=',
                photos: '=',
                mainPhoto: '='
            },
            templateUrl: 'partials/main/about.us.html',
            controller: aboutUsController,
            controllerAs: 'aboutShort',
            bindToController: true
        };

    }]);

    function aboutUsController() {

        var vm = this;
        vm.mainBackground = '{"background-image" : "url('+ vm.mainPhoto +')"}';
    }
}());
