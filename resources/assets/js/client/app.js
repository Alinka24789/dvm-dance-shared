'use strict';

var app = angular
    .module('dvm', ['templates', 'dvm.common', 'dvm.constants', 'dvm.main'])
    .config(['$mdThemingProvider',
        function ($mdThemingProvider) {
            $mdThemingProvider.theme('default')
                .primaryPalette('teal');
        }]);
