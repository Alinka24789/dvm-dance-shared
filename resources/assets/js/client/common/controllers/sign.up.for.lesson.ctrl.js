(function () {
    'use strict';
    angular
    .module('dvm.common')
    .controller('SignUpForLessonController', SignUpForLessonController);

    function SignUpForLessonController($mdDialog, gettextCatalog) {
        var vm = this;
        vm.hide = hide;
        vm.cancel = cancel;
        
        vm.directions = {
            'stretching' : gettextCatalog.getString('stretching'),
            'solo_latina' : gettextCatalog.getString('solo_latina')
        }

        function cancel(){
            $mdDialog.cancel()
        }

        function hide() {
            $mdDialog.hide();
        }
    }


})();
