(function () {
    'use strict';
    angular
        .module('dvm.common')
        .controller('CommonDialogController', CommonDialogController);

    function CommonDialogController($mdDialog, data) {
        var vm = this;
        vm.hide = hide;
        vm.cancel = cancel;

        function cancel(){
            $mdDialog.cancel()
        }

        function hide() {
            $mdDialog.hide();
        }
    }


})();
