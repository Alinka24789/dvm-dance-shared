(function () {
    'use strict';
    angular
    .module('dvm.common')
    .controller('MapDialogController', MapDialogController);

    function MapDialogController($mdDialog) {
        var vm = this;
        vm.hide = hide;
        vm.cancel = cancel;

        init();

        function init() {
            vm.showMap = true;
            vm.center = [48.01, 37.81];
            vm.zoom = 12;

            vm.coords = [48.01, 37.81];
            vm.properties = {balloonContent : 'пр-т Мира, 17', iconColor: '#2e0a05'};
        }


        function cancel(){
            vm.showMap = false;
            $mdDialog.cancel()
        }

        function hide() {
            vm.showMap = false;
            $mdDialog.hide();
        }
    }


})();
