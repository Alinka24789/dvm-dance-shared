(function () {
    'use strict';

    angular
        .module('dvm.common')
        .config(['$urlRouterProvider', '$stateProvider',
            function ($urlRouterProvider, $stateProvider) {

                $stateProvider
                    .state('app', {
                        abstract: true,
                        templateUrl: 'layout.html'
                    })
                    .state('app.main', {
                        url: '/',
                        controller: 'MainPageController',
                        controllerAs: 'vm',
                        templateUrl: 'main.html'
                    })
                    .state('app.404', {
                        templateUrl: 'partials/404.html'
                    });
            }
        ]);
}());
