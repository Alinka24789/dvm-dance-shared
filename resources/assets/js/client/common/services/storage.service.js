(function () {
    'use strict';

    angular
        .module('dvm.common')
        .service('storageService', storageService);

    /**
     * @ngdoc service
     * @name colectik.common:storageService
     * @param $window
     */
    function storageService($window) {

        var prefix = 'colectik-',
            isStorageAvailable = true;

        return {
            set: set,
            get: get,
            remove: remove,
            isAvailable: isLocalStorageAvailable
        };

        /**
         * Add key\value pair for local storage
         * @param key
         * @param data
         */
        function set(key, data) {
            if (isStorageAvailable)
                $window.localStorage && ($window.localStorage[prefix + key] = data);
            return storageService;
        }

        /**
         * Get value from local storage by key
         * @param key
         * @param defaultValue Default value returns when key was not
         * found in storage
         * @returns {Storage|*|null}
         */
        function get(key, defaultValue) {
            if (isStorageAvailable)
                return $window.localStorage && $window.localStorage[prefix + key] || defaultValue || null;
        }

        /**
         * Remove obj from local storage by key
         * @param key
         */
        function remove(key) {
            if (isStorageAvailable)
                $window.localStorage && $window.localStorage.removeItem(prefix + key);

        }

        /**
         * Check for ability read\write to browser's localStorage
         *
         * @returns {boolean} result
         */
        function isLocalStorageAvailable() {
            try {
                localStorage.setItem('test', 'true');
                if (localStorage.getItem('test') === 'true') {
                    localStorage.removeItem('test');
                    isStorageAvailable = true;
                    return true;
                }
            } catch (er) {
                isStorageAvailable = false;
            }
            return false;
        }

    }


})();