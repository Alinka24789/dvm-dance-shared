(function () {
    'use strict';

    angular
        .module('dvm.common')
        .factory('utils', utils);

    /**
     * @ngdoc service
     * @name colectik.common:utils
     * @description
     *
     * */
    function utils($mdDialog, $filter) {

        return {
            orientateCanvas: orientateCanvas,
            getPopupOptions: getPopupOptions,
            showMessageDialog: showMessageDialog,
            replaceLinesWithBr: replaceLinesWithBr,
            replaceAll: replaceAll
        };

        function replaceAll(str, s, d) {
            return str.split(s).join(d);
        }

        function replaceLinesWithBr(text) {
            if (!text) {
                return null;
            }
            var escapedText = $filter('escapeHtml')(text);
            return escapedText.trim().replace(/(?:\r\n|\r|\n)/g, '<br />');
        }

        function orientateCanvas(context, img, orientation) {
            context.canvas.width = img.naturalWidth;
            context.canvas.height = img.naturalHeight;
            switch (orientation) {
                case 2:
                    // horizontal flip
                    context.translate(context.canvas.width, 0);
                    context.scale(-1, 1);
                    break;
                case 3:
                    // 180° rotate left
                    context.translate(context.canvas.width, context.canvas.height);
                    context.rotate(Math.PI);
                    break;
                case 4:
                    // vertical flip
                    context.translate(0, context.canvas.height);
                    context.scale(1, -1);
                    break;
                case 5:
                    // vertical flip + 90 rotate right
                    context.canvas.height = img.naturalWidth;
                    context.canvas.width = img.naturalHeight;
                    context.rotate(0.5 * Math.PI);
                    context.scale(1, -1);
                    break;
                case 6:
                    // 90° rotate right
                    context.canvas.height = img.naturalWidth;
                    context.canvas.width = img.naturalHeight;
                    context.rotate(0.5 * Math.PI);
                    context.translate(0, -context.canvas.width);
                    break;
                case 7:
                    // horizontal flip + 90 rotate right
                    context.rotate(0.5 * Math.PI);
                    context.canvas.height = img.naturalWidth;
                    context.canvas.width = img.naturalHeight;
                    context.translate(context.canvas.height, -context.canvas.width);
                    context.scale(-1, 1);
                    break;
                case 8:
                    // 90° rotate left
                    context.canvas.height = img.naturalWidth;
                    context.canvas.width = img.naturalHeight;
                    context.rotate(-0.5 * Math.PI);
                    context.translate(-context.canvas.height, 0);
                    break;
            }
            context.drawImage(img, 0, 0);
            return context;
        }

        /**
         * @desciprtion Set popup options for centering on window and size
         * @param options
         * @returns {*}
         */
        function getPopupOptions(options) {
            options = options || {};
            var width = options.width || 500;
            var height = options.height || 500;
            var finalOptions = angular.extend({
                width: width,
                height: height,
                left: window.screenX + ((window.outerWidth - width) / 2),
                top: window.screenY + ((window.outerHeight - height) / 2.5)
            }, options);
            //get string from object like width=300,heiht=929
            return _.reduce(finalOptions, function (result, value, key) {
                return result += key + '=' + value + ','
            }, '')
        }

        /**
         * @description show dialog with short messages
         * @param header
         * @param text
         * @returns {*}
         */
        function showMessageDialog(header, text) {
            return $mdDialog.show({
                controller: 'CommonDialogController',
                controllerAs: 'vm',
                templateUrl: 'partials/modals/alert.html',
                parent: angular.element(document.body),
                clickOutsideToClose: false,
                focusOnOpen: false,
                locals: {
                    data: {
                        header: header,
                        text: text
                    }
                }
            })
        }
    }

})();
