(function () {
    'use strict';

    angular
        .module('dvm.common')
        .service('directoriesService', directoriesService);

    /**
     * 
     * @param $resource
     * @param BASE_URL
     * @returns {{getByName: getByName}}
     */
    function directoriesService($resource, BASE_URL) {

        var Directory = $resource(BASE_URL + '/directories/:name',{},{
            'get': { method:'GET', cache: true}
        });

        return {
            getByName: getByName
        };

        function getByName(name) {

            return Directory.get({
                name: name
            }).$promise;
        }
    }


})();
