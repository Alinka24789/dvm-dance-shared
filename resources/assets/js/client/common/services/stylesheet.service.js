(function () {
    'use strict';

    angular
        .module('dvm.common')
        .factory('stylesheets', stylesheets);

    function stylesheets() {

        return {
            createSheet: createSheet,
            addCSSRule: addCSSRule,
        };

        function addCSSRule(sheet, selector, rules, index) {
            if("insertRule" in sheet) {
                sheet.insertRule(selector + "{" + rules + "}", index);
            }
            else if("addRule" in sheet) {
                sheet.addRule(selector, rules, index);
            }
        }

        function createSheet() {
            // Create the <style> tag
            var style = document.createElement("style");
            // Add a media (and/or media query) here if you'd like!
            // style.setAttribute("media", "screen")
            // style.setAttribute("media", "only screen and (max-width : 1024px)")
            // WebKit hack :(
            style.appendChild(document.createTextNode(""));
            // Add the <style> element to the page
            document.head.appendChild(style);
            return style.sheet;
        }

    }
}());
