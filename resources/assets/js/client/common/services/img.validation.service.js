(function () {
  'use strict';

  angular
    .module('dvm.common')
    .service('imageValidationService', imageValidationService);

  /**
   * @ngdoc service
   * @name colectik.common:imageValidationService
   */
  function imageValidationService($q, $log, gettextCatalog, utils) {

    var requirements = {
      maxImgFileSize: 10 * 1024 * 1024,
      minWidth: 300,
      minHeight: 300,
      regExp: /(png|jpg|jpeg)$/
    };
    return {
      validate: validate
    };

    /**
     *
     * @param imgFile File from file input
     * @param passedRequirements Object with requirements to image
     *  {maxImgFileSize: number,
         *  minWidth: number,
         *  minHeight: number}
     * @returns {*}
     */
    function validate(imgFile, passedRequirements) {
      if (passedRequirements) {
        requirements = angular.extend({}, requirements, passedRequirements);
      } 
      var delay = $q.defer();
      if (!imgFile) {
        $log.debug('No image file specified', imgFile);
        return $q.reject({
          //reason: 'noFile'
          reason: gettextCatalog.getString('noFile')
        });
      }
      if (!requirements.regExp.test(imgFile.type)) {
        $log.debug('Incorrect image format', imgFile);
        return $q.reject({
          //reason: 'Incorrect file format'
          reason: gettextCatalog.getString('incorrect_image_format')
        })
      }
      if (imgFile.size > requirements.maxImgFileSize) {
        $log.debug('Incorrect image size', imgFile);
        return $q.reject({
          //reason: 'Incorrect image file size'
          reason: gettextCatalog.getString('Incorrect_img_file_size')
        })
      }
      var reader = new FileReader();
      reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
          if (img.naturalHeight < requirements.minHeight || img.naturalWidth < requirements.minWidth) {
            var str = gettextCatalog.getString('Image_size_is_less_than') + ' ';
            delay.reject({
              reason: str + requirements.minWidth + 'x' + requirements.minHeight + 'px'
            });
          }
          delay.resolve({
            file: imgFile,
            path: img.src,
            //path: getImageOrientation(img)
          })
        };
        img.src = event.target.result;
      };
      reader.readAsDataURL(imgFile);
      return delay.promise;
    }


    function getImageOrientation(img) {
      var result = null;
      EXIF.getData(img, function () {
        var orientation = EXIF.getTag(this, 'Orientation');
        var context = document.createElement('canvas').getContext('2d');
        context = utils.orientateCanvas(context, img, orientation);
        result = context.canvas.toDataURL();
      });
      return result;
    }
  }

}());
