(function () {
    'use strict';

    angular
        .module('dvm.common')
        .service('lazyLoadingService', lazyLoadingService);

    /**
     * @ngdoc service
     * @name colectik.common:lazyLoadingService
     */
    function lazyLoadingService($q, $log) {
        return {
            getNewInstance: getNewInstance
        };

        function getNewInstance(fetchFunction, params) {
            if (!_.isFunction(fetchFunction)) {
                $log.error('You need specify fetchFunction for lazyLoading service');
                throw new Error('No FetchFunction Specified');
            }
            return new LazyLoading(fetchFunction, params);
        }

        //Each time need create new instance of factory with self pagination parameters and fetchFunc
        function LazyLoading(fetchFunction, params) {
            var self = this;
            this.params = {
                per_page: 20,
                current_page: 1
            };
            if (params && _.isObject(params) && Object.keys(params).length) {
                angular.extend(self.params, params);
            }

            this.loadMore = function (requestData) {
                if (self.params.current_page > self.params.last_page) {
                    return $q.reject();
                } else {
                    var data = angular.extend({
                        page: self.params.current_page,
                        per_page: self.params.per_page
                    }, requestData);
                    return fetchFunction(data).$promise
                        .then(self._updatePaginationParams);
                }
            };

            this.resetParams = function (data) {
                self.params = angular.extend({
                    per_page: self.params.per_page || 20,
                    current_page: 1
                }, data)
            };

            this._updatePaginationParams = function (result) {
                self.params.last_page = result.data.last_page;
                self.params.next_page_url = result.data.next_page_url;
                self.params.prev_page_url = result.data.prev_page_url;
                self.params.to = result.data.to;
                self.params.total = result.data.total;
                self.params.current_page += 1;
                //tell controller that no more items in pagination
                if (self.params.current_page > self.params.last_page) {
                    result.noMoreItems = true;
                }
                return result;
            };
        }

    }

}());
