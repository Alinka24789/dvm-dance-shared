(function () {
    'use strict';

    angular
        .module('dvm.common')
        .factory('netUtils', netUtils);

    function netUtils($rootScope, $state) {

        return {
            handleNotFound: handleNotFound
        };

        function handleNotFound(reason) {
            if (reason && reason.status == 404) {
                $rootScope.stateLoader = false;
                $state.go('app.404', {
                    reload: false
                }, {
                    location: false
                });
            }
        }
    }

}());
