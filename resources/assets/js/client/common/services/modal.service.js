(function () {
    'use strict';

    angular
    .module('dvm.common')
    .factory('modalService', validationService);


    function validationService($mdDialog) {

        return {
            showSignUpForLessonModal: showSignUpForLessonModal,
            showMapModal: showMapModal
        };
        
        function showSignUpForLessonModal() {
            var options = {
                controllerAs: 'vm',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                controller: 'SignUpForLessonController',
                templateUrl: 'partials/modals/sign.up.for.lesson.html',
                locals: {}
            };
            return $mdDialog.show(options);
        }

        function showMapModal() {
            var options = {
                controllerAs: 'vm',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                controller: 'MapDialogController',
                templateUrl: 'partials/modals/map.html',
                locals: {}
            };
            return $mdDialog.show(options);
        }
    }

})();