(function () {
    'use strict';

    angular
        .module('dvm.common')
        .factory('validationService', validationService);

    /**
     * @ngdoc service
     * @name colectik.authorizaiton:validationService
     * @description Validate forms input after server responce with error
     * Sample
     * {"success":false,"error":{"message":"Validation failed",
     * "code":12,"description":"Some fields are not valid. See details below","
     * details":[{"field":"email","code":2003,"description":"The email has already been taken"}]}}
     */
    function validationService() {

        var ERROR_CODE_PASSWORD_CONFIRMATION = 1019;

        return {
            validate: validate
        };

        /**
         *
         * @param form angular form obj
         * @param err Response with errors
         * @param serverErrors Array to store error messages
         */
        function validate(form, err, serverErrors) {
            if (_.get(err, 'data.error.details[0].description')) {
                var errors = _.get(err, 'data.error.details');
                if (errors.length) {
                    _.each(errors, function (item) {
                        if (form[item.field]) {
                            if (item.code == ERROR_CODE_PASSWORD_CONFIRMATION) {
                                form['password_confirmation'].$setValidity('servererror', false);
                                serverErrors['password_confirmation'] = item.description;
                            } else {
                                form[item.field].$setValidity('servererror', false);
                                serverErrors[item.field] = item.description;
                            }
                        }
                    });
                }
            }
        }
    }
    
})();