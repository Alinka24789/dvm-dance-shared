(function () {
    'use strict';

    angular
        .module('dvm.common')
        .factory('HttpInterceptor', HttpInterceptor);

    /**
     * @ngdoc service
     * @name colectik.common:httpInterceptor
     * @see app.common.js HTTP_EVENTS
     */
    function HttpInterceptor($rootScope, $q, $log, HTTP_EVENTS, BASE_URL) {

        var DEFAULT_REQUEST_TIMEOUT = 30000; //after timeout cancel pending requests from client
        var activeRequestsMap = {};

        return {
            responseError: responseError,
            request: request,
            response: response,
            requestError: requestError
        };

        function response(response){
            if (_.has(response, 'config.url') &&  activeRequestsMap[response.config.url]) {
                delete activeRequestsMap[response.config.url]
            }
            return response;
        }

        function requestError(rejection){
            return $q.reject(rejection);
        }


        function request(config) {
            if (config && config.url && config.url.indexOf(BASE_URL) > -1) {
                if (activeRequestsMap[config.url]) {
                    return $q.reject({status: 504});
                }
                activeRequestsMap[config.url] = true;
            }
            config.timeout = DEFAULT_REQUEST_TIMEOUT;
            return config;
        }

        /**
         * Intercept server errors, on auth errors sends event to services
         * and log error to console
         * @param {promise} rejection
         * @returns {promise}
         */
        function responseError(rejection) {
            if (_.has(rejection, 'config.url') && activeRequestsMap[rejection.config.url]) {
                delete activeRequestsMap[rejection.config.url]
            }
            $rootScope.$broadcast({
                '-1': HTTP_EVENTS.timeout,
                '401': HTTP_EVENTS.notAuthenticated,
                '403': HTTP_EVENTS.notAuthorized,
                '404': HTTP_EVENTS.notFound,
                '405': HTTP_EVENTS.notAllowed,
                '422': HTTP_EVENTS.unprocessableEntity,
                '423': HTTP_EVENTS.blocked,
                '500': HTTP_EVENTS.serverError
            }[rejection.status], rejection);
            return $q.reject(rejection);
        }
    }


})();
