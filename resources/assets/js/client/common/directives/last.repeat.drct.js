(function () {
    'use strict';

    angular
        .module('dvm.common')
        .directive('lastRepeat', lastRepeat);

    function lastRepeat($timeout) {
        return function (scope) {
            var EVENT_TIMEOUT = 100; //TODO scope deep level
            if (scope.$last) {
                if (scope.$parent && scope.$parent.$parent) {
                    $timeout(function () {
                        scope.$parent.$parent.$broadcast('last-repeat');
                    }, EVENT_TIMEOUT);

                }
            }
        }
    }

}());
