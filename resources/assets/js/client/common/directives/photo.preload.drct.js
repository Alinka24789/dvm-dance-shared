(function () {
    'use strict';

    angular.module('dvm.common')
        .directive('photoPreload', photoPreload);

    /**
     * Display default image while image loading and on error display dummy img
     */
    function photoPreload() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var parent = element.parent();

                init();

                function init() {
                    if (attrs.originSrc) {
                        parent.addClass('image-loading');
                        var img = new Image();
                        img.src = attrs.originSrc;
                        img.onload = function () {
                            attrs.$set('src', attrs.originSrc);
                            parent.removeClass('image-loading');
                            parent.removeClass('image-error')

                        };
                        img.onerror = function () {
                            parent.removeClass('image-loading')
                            parent.addClass('image-error');
                        }
                    }
                    else {
                        parent.removeClass('image-loading')
                        parent.addClass('image-error');
                    }
                }

                attrs.$observe('originSrc', function (value, old) {
                    if (value !== old) {
                        init();
                    }
                });

            }
        }
    }

})();
