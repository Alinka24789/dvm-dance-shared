(function () {
    'use strict';

    angular
        .module('dvm.common')
        .run(['$rootScope', '$log', '$timeout', '$location', 'gettextCatalog',
            function ($rootScope, $log, $timeout, $location, gettextCatalog) {

                $rootScope.location = $location;

                gettextCatalog.setCurrentLanguage('ru');
            }
        ]);
}());
