(function () {
    'use strict';

    angular
        .module('dvm.common', ['ngResource', 'ngMaterial', 'ui.router', 'gettext', 'ngAnimate', 'ngSanitize', 'ngMessages', 'ymaps'])
        .config(['$stateProvider', '$locationProvider', '$httpProvider', '$urlRouterProvider', '$mdIconProvider', '$mdThemingProvider',
            function ($stateProvider, $locationProvider, $httpProvider, $urlRouterProvider, $mdIconProvider, $mdThemingProvider) {

                $mdThemingProvider.definePalette('dvmLight', {
                    '50': 'ffd394',
                    '100': 'ffd394',
                    '200': 'ffd394',
                    '300': 'ffd394',
                    '400': 'ffd394',
                    '500': 'ffd394',
                    '600': 'ffd394',
                    '700': 'ffd394',
                    '800': 'ffd394',
                    '900': 'ffd394',
                    'A100': 'ffd394',
                    'A200': 'ffd394',
                    'A400': 'ffd394',
                    'A700': 'ffd394',
                    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                                        // on this palette should be dark or light
                    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
                        '200', '300', '400', 'A100'],
                    'contrastLightColors': undefined    // could also specify this if default was 'dark'
                });
                $mdThemingProvider.theme('default')
                .primaryPalette('brown')
                .accentPalette('dvmLight');

                $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                });
                $mdIconProvider
                    .defaultIconSet('/img/svg/mdi.svg');

                $httpProvider.interceptors.push('HttpInterceptor');
                $urlRouterProvider.otherwise(function ($injector, $location) {

                    var state = $injector.get('$state');
                    return $location.path();
                });

            }])
        .constant('VALIDATION_ERROR_CODE', 12)
        .constant('HTTP_EVENTS', {
            notAuthenticated: 'auth-not-authenticated', //401
            notAuthorized: 'auth-not-authorized', //403
            notFound: 'http-404', //404
            notAllowed: 'http-405', //405
            timeout: 'http-408', //405
            unprocessableEntity: 'unprocessable-entity', //422
            blocked: 'auth-user-blocked', //423
            serverError: 'server-error' //500
        })
        .constant('SERVER_STATUS', {
            'created': 201,
            'ok': 200
        });
}());
