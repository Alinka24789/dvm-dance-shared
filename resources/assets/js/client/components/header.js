(function () {
    'use strict';

    angular
        .module('dvm.common')
        .component('appHeader', {

            bindings: {},
            controller: 'HeadController',
            controllerAs: 'head',
            templateUrl: 'base/header.html'
        })
        .controller('HeadController', HeadController);

    function HeadController($rootScope, $location, $mdSidenav, $timeout, $state, modalService) {
        var vm = this;
        var SIDENAV_ANIMATION_DELAY = 250;
        vm.toggleSidenav = toggleSidenav;
        vm.showSignUpForLessonModal = showSignUpForLessonModal;
        
        function toggleSidenav(stateName) {
            $mdSidenav('left').toggle();
            if (stateName) {
                $timeout(function () {
                    $state.go(stateName)
                }, SIDENAV_ANIMATION_DELAY);
            }
        }

        $rootScope.gotoAnchor = function(x) {
            var newHash = 'anchor' + x;
            if ($location.hash() !== newHash) {
                // set the $location.hash to `newHash` and
                // $anchorScroll will automatically scroll to it
                $location.hash('anchor' + x);
            } else {
                // call $anchorScroll() explicitly,
                // since $location.hash hasn't changed
                $anchorScroll();
            }
        };

        function showSignUpForLessonModal() {
            console.log('yew main header');
            return modalService.showSignUpForLessonModal();
        }

    }

}());
