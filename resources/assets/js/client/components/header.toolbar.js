(function () {
    'use strict';

    angular
        .module('dvm.common')
        .directive('headerToolbar', [function() {
            return {
                restrict: 'E',
                replace: true,
                scope: false,
                controller: HeaderToolbarController,
                controllerAs: 'toolbarCtrl',
                templateUrl: 'base/header.toolbar.html'
            }
        }]);

    function HeaderToolbarController(modalService) {

        var vm = this;
        vm.searchValue = '';
        vm.showSignUpForLessonModal = showSignUpForLessonModal;
        vm.showMap = showMap;

        function showSignUpForLessonModal() {
            return modalService.showSignUpForLessonModal();
        }
        
        function showMap() {
            return modalService.showMapModal();
        }
    }

}());
