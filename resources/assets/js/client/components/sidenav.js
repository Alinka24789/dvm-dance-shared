(function () {
    'use strict';

    angular
        .module('dvm.common')
        .component('sidenav', {
            bindings: {},
            controller: sidenavController,
            controllerAs: 'head',
            templateUrl: 'base/sidenav.html'
        });

    function sidenavController($mdSidenav, $timeout, $state, modalService) {
        var vm = this;
        var SIDENAV_ANIMATION_DELAY = 250;
        vm.toggleSidenav = toggleSidenav;
        vm.showSignUpForLessonModal = showSignUpForLessonModal;

        function showSignUpForLessonModal() {
            return modalService.showSignUpForLessonModal();
        }

        function toggleSidenav(stateName) {
            $mdSidenav('left').toggle();
            if (stateName) {
                $timeout(function () {
                    $state.go(stateName)
                }, SIDENAV_ANIMATION_DELAY);
            }
        }
        function _closeSidenav() {
            if ($mdSidenav('left').isOpen()) {
                $mdSidenav('left').close();
            }
        }
    }

}());
