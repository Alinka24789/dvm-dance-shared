<!doctype html>
<html lang="ru" ng-app="dvm">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="fragment" content="!">
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <!--JS meta-->
    <title ng-bind="meta.title"></title>
    <meta name="title" content="@{{meta['title']}}" />
    <meta name="description" content="@{{meta['description']}}" />
    <meta name="keywords" content="@{{meta['keywords']}}" />
    <meta name="og:title" content="@{{meta['og:title']}}" />
    <meta name="og:description" content="@{{meta['og:description']}}" />
    <meta name="og:image" content="@{{meta['og:image']}}" />
    <meta name="og:image:secure_url" content="@{{meta['og:image:secure_url']}}" />
    <meta property="og:type" content="@{{meta['og:type']}}" />
    <meta property="og:locale" content="@{{meta['og:locale']}}" />
    <meta name="og:image:width" content="@{{meta['og:image:width']}}" />
    <meta name="og:image:height" content="@{{meta['og:image:height']}}" />
    <meta name="fb:app_id" content="@{{meta['fb:app_id']}}" />
    <meta name="twitter:card" content="@{{meta['twitter:card']}}" />
    <meta name="twitter:site" content="@dvm_test"/>
    <meta name="twitter:title" content="@{{meta['title']}}" />
    <meta name="twitter:description" content="@{{meta['description']}}"  />
    <meta name="twitter:text:description" content="@{{meta['description']}}"  />
    <meta name="twitter:image" content="@{{meta['og:image']}}" />
    <meta name="robots"  content="@{{meta['robots']}}"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{config('app.static_media_url')}}{{ elixir('css/client/vendor.css') }}">
    <link rel="stylesheet" href="{{config('app.static_media_url')}}{{ elixir('css/client/user.css') }}">
    <script src="{{config('app.static_media_url')}}{{ elixir('js/client/vendor.js') }}"></script>
    <script src="{{config('app.static_media_url')}}{{ elixir('js/client/user.js') }}"></script>


    <link rel="icon" type="image/png" href="{{config('app.static_media_url')}}/favicon.ico">
    <style type="text/css">
        /**
         * Hide when Angular is not yet loaded and initialized
         */
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak, .ng-hide:not(.ng-hide-animate) {
            display: none !important;
        }
    </style>
</head>
<body>

<ui-view></ui-view>

<script>
    angular
    .module('dvm.constants', [])
    .constant('BASE_URL', '{{config('app.api_full_url')}}')
    .constant('AGE', {
        'min_age': '{{config('app.settings_profile.min_age')}}',
        'max_age': '{{config('app.settings_profile.max_age')}}',
    })
    .constant('SITE_URL', '{{config('app.site_url')}}')
</script>
</body>
</html>
