<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// for angular
Route::get('/{angularPart}', function () {
    return view('welcome');
})->where('angularPart', '^(?!(\/?api\/|\/?admin\/|\/?uploads\/)).*$');
